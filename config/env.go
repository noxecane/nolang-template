package config

import (
	"github.com/joho/godotenv"
	"github.com/kelseyhightower/envconfig"
	"tsaron.com/cast/vendors"
)

// Env is the expected config values from the process's environment
type Env struct {
	// Basic Application config
	AppEnv string `default:"dev" split_words:"true"`
	Name   string `default:"cast"`
	Port   int    `required:"true"`
	Scheme string `required:"true"`
	Secret string `required:"true"`
	vendors.PostgresEnv
	vendors.RedisEnv
}

// LoadEnv loads environment variables into Env
func LoadEnv() (Env, error) {
	var config Env

	// try to load from .env first
	err := godotenv.Load()
	if err != nil {
		return config, err
	}

	err = envconfig.Process("", &config)
	if err != nil {
		return config, err
	}

	return config, nil
}
