package config

import (
	"os"

	"github.com/go-pg/pg/v9"
	"github.com/go-redis/redis/v7"
	"github.com/rs/zerolog"
	"tsaron.com/cast/vendors"
)

// App is basically the global container for resources
type App struct {
	Db    *pg.DB
	Store *redis.Client
	zerolog.Logger
	Env
}

// NewApp loads all resources
func NewApp() *App {
	host, err := os.Hostname()
	if err != nil {
		panic(err)
	}

	env, err := LoadEnv()
	if err != nil {
		panic(err)
	}

	log := zerolog.
		New(os.Stdout).
		With().
		Timestamp().
		Str("service", env.Name).
		Str("host", host).
		Logger()

	db := vendors.ConnectToPostgres(env.PostgresEnv)
	log.Info().Msg("Connected to postgres successfully")

	store := vendors.ConnectToRedis(env.RedisEnv)
	log.Info().Msg("Connected to redis successfully")

	return &App{
		Db:     db,
		Store:  store,
		Logger: log,
		Env:    env,
	}
}

// Close all resources
func (app *App) Close() {
	err := app.Db.Close()
	if err != nil {
		app.Fatal().Err(err).Msg("Cannot shutdown postgres connection")
	}

	err = app.Store.Close()
	if err != nil {
		app.Fatal().Err(err).Msg("Cannot shutdown redis connection")
	}
}
