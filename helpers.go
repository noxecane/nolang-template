package main

import (
	"context"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"time"

	"tsaron.com/cast/config"
)

func listenForInterrupt(app *config.App, cancel context.CancelFunc) {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)

	// wait for Quit signal
	<-quit

	// cancel context so dependencies can gracefully shutdown
	app.Info().Msg("Signal caught. Shutting down...")
	cancel()
}

func waitForContext(ctx context.Context, app *config.App, server *http.Server) chan struct{} {
	// listen for shutdown signal from the context so we can shutdown the server
	done := make(chan struct{})
	go func() {
		<-ctx.Done()
		if err := server.Shutdown(context.Background()); err != nil {
			app.Err(err).Msg("Could not shut down server cleanly.....")
		}
		close(done)
	}()

	return done
}

func runServerFor(app *config.App, handler http.Handler) {
	// request context
	ctx, cancel := context.WithCancel(context.Background())
	server := &http.Server{
		Addr:        fmt.Sprintf(":%d", app.Env.Port),
		Handler:     handler,
		ReadTimeout: 2 * time.Minute,
	}

	go listenForInterrupt(app, cancel)
	done := waitForContext(ctx, app, server)

	app.Info().Msgf("Serving api at http://127.0.0.1:%d", app.Env.Port)
	if err := server.ListenAndServe(); err != http.ErrServerClosed {
		app.Err(err).Msg("Could not start the server")
	}

	// return only when done is closed
	<-done
}
