package controllers

import (
	"net/http"
	"time"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"tsaron.com/cast/config"
	"tsaron.com/cast/vendors"
)

// APIRoutes is the general handler for all routes in this application
var APIRoutes *chi.Mux

func init() {
	APIRoutes = chi.NewRouter()
}

// SetupMiddleware adds global middleware to the router
func SetupMiddleware(app *config.App) {
	appEnv := app.AppEnv
	if appEnv == "dev" {
		APIRoutes.Use(vendors.DevCORS().Handler)
	} else {
		APIRoutes.Use(
			vendors.SecureCORS(
				"https://*godview.netlify.com",
				"https://*tsaron.com",
				"http://localhost").
				Handler)
	}

	APIRoutes.Use(middleware.RequestID)
	APIRoutes.Use(middleware.RealIP)
	APIRoutes.Use(vendors.ZeroMiddleware(app.Logger, app.AppEnv))
	APIRoutes.Use(middleware.Recoverer)
	APIRoutes.Use(middleware.RedirectSlashes)
	APIRoutes.Use(middleware.Compress(5))
	APIRoutes.Use(middleware.Timeout(time.Second * 60))

	APIRoutes.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "text/plain")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("Up and Running!"))
	})

	APIRoutes.NotFound(func(w http.ResponseWriter, r *http.Request) {
		http.Error(w, "Whoops!! This route doesn't exist", 404)
	})

	APIRoutes.Route("/api/v1", func(r chi.Router) {
		// r.Get("/users")
	})
}
