package main

import (
	"tsaron.com/cast/config"
	"tsaron.com/cast/controllers"
)

func main() {
	app := config.NewApp()
	controllers.SetupMiddleware(app)
	runServerFor(app, controllers.APIRoutes)
}
