package vendors

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/go-chi/chi/middleware"
	"github.com/rs/zerolog"
)

var kubeProbe = regexp.MustCompile("(?i)kube-probe|prometheus")

// ZeroLogger is a wrapper around zero log for chi
type ZeroLogger struct {
	BaseLog zerolog.Logger
	Env     string
}

// ZeroLogEntry is an info event for request detauis
type ZeroLogEntry struct {
	Log *zerolog.Logger
}

// APIError is a struct describing an error
type APIError struct {
	Message string                 `json:"message"`
	Meta    map[string]interface{} `json:"meta"`
}

// ZeroMiddleware creates a middleware for logging http Requests
func ZeroMiddleware(log zerolog.Logger, env string) func(next http.Handler) http.Handler {
	return middleware.RequestLogger(ZeroLogger{BaseLog: log, Env: env})
}

// Panic logs final requests that failed with a panic
func (e *ZeroLogEntry) Panic(v interface{}, stack []byte) {
	e.Log.UpdateContext(func(ctx zerolog.Context) zerolog.Context {
		return ctx.
			Str("stack", string(stack)).
			Str("panic", fmt.Sprintf("%+v", v))
	})
}

// Write logs the response metadata for a request
func (e *ZeroLogEntry) Write(status, bytes int, elapsed time.Duration) {
	e.Log.
		Info().
		Int("status", status).
		Int("length", bytes).
		Float64("elapsed", float64(elapsed.Milliseconds())).
		Msg("")
}

// NewLogEntry creates a special log for each request and storing it's request
// info for write logs or panic logs
func (l ZeroLogger) NewLogEntry(r *http.Request) middleware.LogEntry {
	newLogger := l.BaseLog.With().Logger()
	entry := &ZeroLogEntry{Log: &newLogger}

	if kubeProbe.MatchString(r.UserAgent()) {
		return entry
	}

	if reqID := middleware.GetReqID(r.Context()); reqID != "" {
		newLogger.UpdateContext(func(ctx zerolog.Context) zerolog.Context {
			return ctx.Str("id", reqID)
		})
	}

	lowerCaseHeaders := make(map[string]interface{})

	for k, v := range r.Header {
		lowerKey := strings.ToLower(k)
		if len(v) == 0 {
			lowerCaseHeaders[lowerKey] = ""
		} else if len(v) == 1 {
			lowerCaseHeaders[lowerKey] = v[0]
		} else {
			lowerCaseHeaders[lowerKey] = v
		}
	}

	newLogger.UpdateContext(func(ctx zerolog.Context) zerolog.Context {
		return ctx.
			Str("method", r.Method).
			Str("remote_address", r.RemoteAddr).
			Str("url", r.URL.String()).
			Interface("headers", lowerCaseHeaders)
	})

	if l.Env != "production" {
		requestBody := ReadBody(r)

		if len(requestBody) == 0 {
			return entry
		}

		entry.Log.UpdateContext(func(ctx zerolog.Context) zerolog.Context {
			return ctx.Bytes("request", requestBody)
		})
	}

	return entry
}

// ReadBody extracts the bytes in a request body without destroying the
// contents of the body
func ReadBody(r *http.Request) []byte {
	var buffer bytes.Buffer

	// copy request body to in memory buffer while being read
	readSplit := io.TeeReader(r.Body, &buffer)
	body, err := ioutil.ReadAll(readSplit)
	if err != nil {
		// never run away with your wife
		panic(err)
	}

	// return what you collected
	r.Body = ioutil.NopCloser(&buffer)

	return body
}

// Send response writes a JSON encoded version of `v` to the writer, making
// sure what deserves to be logged gets logged
func Send(env string, r *http.Request, w http.ResponseWriter, v interface{}) {
	raw, err := json.Marshal(v)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	entry := middleware.GetLogEntry(r).(*ZeroLogEntry)

	if apiErr, ok := v.(APIError); ok {
		entry.Log.UpdateContext(func(ctx zerolog.Context) zerolog.Context {
			return ctx.Str("error_message", apiErr.Message)
		})
	}

	if env != "production" && v != nil {
		entry.Log.UpdateContext(func(ctx zerolog.Context) zerolog.Context {
			return ctx.Bytes("response", raw)
		})
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(raw)
}
