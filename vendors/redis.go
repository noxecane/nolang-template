package vendors

import (
	"fmt"

	"github.com/go-redis/redis/v7"
)

// RedisEnv is the definition of environment variables needed
// to connect to redis
type RedisEnv struct {
	RedisHost     string `required:"true" split_words:"true"`
	RedisPort     int    `required:"true" split_words:"true"`
	RedisPassword string `default:""`
}

// ConnectToRedis creates a client for redis operations
func ConnectToRedis(env RedisEnv) *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%d", env.RedisHost, env.RedisPort),
		Password: env.RedisPassword,
		DB:       0,
	})
}
