package vendors

import (
	"crypto/tls"
	"fmt"

	"github.com/go-pg/pg/v9"
)

// PostgresEnv is the definition of environment variables needed
// to setup a postgres connection
type PostgresEnv struct {
	PostgresHost       string `required:"true" split_words:"true"`
	PostgresPort       int    `required:"true" split_words:"true"`
	PostgresSecureMode bool   `required:"true" split_words:"true"`
	PostgresUser       string `required:"true" split_words:"true"`
	PostgresPassword   string `required:"true" split_words:"true"`
	PostgresDatabase   string `required:"true" split_words:"true"`
}

// ConnectToPostgres creates a connection to a postgres DB
func ConnectToPostgres(env PostgresEnv) *pg.DB {
	opts := &pg.Options{
		Addr:     fmt.Sprintf("%s:%d", env.PostgresHost, env.PostgresPort),
		User:     env.PostgresUser,
		Password: env.PostgresPassword,
		Database: env.PostgresDatabase,
	}

	if env.PostgresSecureMode {
		opts.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	}

	return pg.Connect(opts)
}
