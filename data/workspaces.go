package data

import (
	"time"

	"github.com/go-pg/pg/v9"
)

type workspaceRepository struct {
	Db *pg.DB
}

// Position is a data unit form
type Position struct {
	tableName  struct{} `pg:"workspaces"`
	Id         int
	CreatedAt  time.Time `pg:"default:now()"`
	DeviceTime time.Time `pg:"devicetime"`
	Attributes string
}

func (repo workspaceRepository) CreateWorkspace() {

}
