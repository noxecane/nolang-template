module tsaron.com/cast

go 1.13

require (
	github.com/go-chi/chi v4.0.3+incompatible
	github.com/go-pg/pg/v9 v9.1.3
	github.com/go-redis/redis/v7 v7.2.0
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/rs/cors v1.7.0
	github.com/rs/zerolog v1.18.0
	golang.org/x/net v0.0.0-20200301022130-244492dfa37a // indirect
	golang.org/x/sys v0.0.0-20200302150141-5c8b2ff67527 // indirect
)
